package net.pl3x.bukkit.homes.configuration;

import net.pl3x.bukkit.homes.Pl3xHomes;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;
    public static boolean USE_TELEPORT_SOUNDS;
    public static Sound SOUND_TO;
    public static Sound SOUND_FROM;

    public static void reload(Pl3xHomes plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        USE_TELEPORT_SOUNDS = config.getBoolean("use-teleport-sounds", true);
        try {
            SOUND_TO = Sound.valueOf(config.getString("sound-to", "ENTITY_ENDERMEN_TELEPORT"));
        } catch (IllegalArgumentException e) {
            plugin.getLog().warn("Invalid \"sound-to\" config value. Using \"ENTITY_ENDERMEN_TELEPORT\" instead.");
            SOUND_TO = Sound.ENTITY_ENDERMEN_TELEPORT;
        }
        try {
            SOUND_FROM = Sound.valueOf(config.getString("sound-from", "ENTITY_ENDERMEN_TELEPORT"));
        } catch (IllegalArgumentException e) {
            plugin.getLog().warn("Invalid \"sound-from\" config value. Using \"ENTITY_ENDERMEN_TELEPORT\" instead.");
            SOUND_FROM = Sound.ENTITY_ENDERMEN_TELEPORT;
        }
    }
}
