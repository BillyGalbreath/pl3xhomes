package net.pl3x.bukkit.homes;

import net.pl3x.bukkit.homes.commands.CmdDelHome;
import net.pl3x.bukkit.homes.commands.CmdHome;
import net.pl3x.bukkit.homes.commands.CmdHomes;
import net.pl3x.bukkit.homes.commands.CmdPl3xHomes;
import net.pl3x.bukkit.homes.commands.CmdSetHome;
import net.pl3x.bukkit.homes.configuration.Config;
import net.pl3x.bukkit.homes.configuration.Lang;
import net.pl3x.bukkit.homes.configuration.PlayerConfig;
import net.pl3x.bukkit.homes.hook.Vault;
import net.pl3x.bukkit.homes.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xHomes extends JavaPlugin {
    private final Logger logger;

    public Pl3xHomes() {
        this.logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        if (!Bukkit.getPluginManager().isPluginEnabled("Vault")) {
            getLog().error("Missing required dependency: Vault");
            return;
        }

        if (!Vault.setupPermissions()) {
            getLog().error("Vault could not find a permissions plugin to hook to!");
            return;
        }

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("delhome").setExecutor(new CmdDelHome(this));
        getCommand("home").setExecutor(new CmdHome(this));
        getCommand("homes").setExecutor(new CmdHomes(this));
        getCommand("sethome").setExecutor(new CmdSetHome(this));
        getCommand("pl3xhomes").setExecutor(new CmdPl3xHomes(this));

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        PlayerConfig.removeAll();

        getLog().info(getName() + " disabled!");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }
}
